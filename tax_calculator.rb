class TaxCalculator
  attr_reader :income

  TAX_BRACKETS = {
    (1..45_916) => 15,
    (45_917..91_831) => 20.5,
    (91_832..142_353) => 26,
    (142_354..202_800) => 29,
    (202_801..Float::INFINITY) => 33
   }

  def initialize(income_string)
    @income = income_string.gsub(/\D/, '').to_i
    @amount_owed = 0
  end

  def tax_amount_due
    calculate_tax_amount_due
    "$#{number_to_delimited_number(@amount_owed.round(2))}"
  end

  private

  def calculate_tax_amount_due
    TAX_BRACKETS.each do |income_range, tax_rate|
      amount_to_tax = (income_range.begin..[@income, income_range.end].min).size
      break if amount_to_tax <= 0
      @amount_owed += amount_to_tax * (tax_rate/100.0)
    end
  end

  def number_to_delimited_number(number)
    parts = number.to_s.split('.')
    parts[0].gsub!(/(\d)(?=(\d\d\d)+(?!\d))/, "\\1,")
    parts.join('.')
  end
end


puts TaxCalculator.new('99999831').tax_amount_due
