Write a function in a language of your choice that accepts a taxable income (string) and returns tax owed (string) given the following marginal tax rates:

15% on the first $45,916 of taxable income, +
20.5% on the next $45,915 of taxable income (the portion of taxable income over $45,916 up to $91,831), +
26% on the next $50,522 of taxable income (the portion of taxable income over $91,831 up to $142,353), +
29% on the next $60,447 of taxable income (the portion of taxable income over $142,353 up to $202,800), +
33% of taxable income over $202,800.
