require 'rspec'
require_relative 'tax_calculator.rb'

describe "TaxCalculator" do
  describe ".tax_amount_due" do
    let(:result) { TaxCalculator.new('$202,800').tax_amount_due }

    it "should return a string of the tax amount due" do
      expect(result).to be_a(String)
      expect(result).to eq('$46,965.33')
    end
  end
end
